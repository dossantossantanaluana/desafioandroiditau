package com.example.desafioandroiditauluana.data

import com.example.desafioandroiditauluana.data.model.Category
import com.example.desafioandroiditauluana.data.model.Transaction

sealed class Results {
    class SuccessCategory(val transactions: List<Category>) : Results()
    class ApiError(val statusCode: Int) : Results()
    object ServerError : Results()
    class SuccessTransaction(val transactions: List<Transaction>) : Results()
}