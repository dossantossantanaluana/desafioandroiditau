package com.example.desafioandroiditauluana.data

import android.view.SurfaceControl
import com.example.desafioandroiditauluana.data.model.Category
import com.example.desafioandroiditauluana.data.model.Transaction
import retrofit2.Call
import retrofit2.http.GET

interface Services {

    @GET("lancamentos/")
    fun getTransactions(): Call<List<Transaction>>

    @GET("categorias/")
    fun getCategories(): Call<List<Category>>
}