package com.example.desafioandroiditauluana.data.repository

import com.example.desafioandroiditauluana.data.Results

interface CategoriesRepository {

    fun getCategories(resultCallback: (result: Results) -> Unit)
}