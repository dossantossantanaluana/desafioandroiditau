package com.example.desafioandroiditauluana.data.repository

import com.example.desafioandroiditauluana.data.Results

interface TransactionsRepository {

    fun getTransactions(resultCallback: (result: Results) -> Unit)
}